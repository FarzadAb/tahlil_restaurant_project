import os
import django
import schedule
import time
from jdatetime import datetime, timedelta

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "RAS.settings")
django.setup()
os.environ['TZ'] = 'Asia/Tehran'
time.tzset()

from main.models import PeriodicOrder
from django.db import transaction
from scripts.task import createOrderTaskChain


@transaction.atomic
def placePeriodicOrder(periodicOrder):
    periodicOrder.customer.accountCredit -= periodicOrder.totalPrice()
    if periodicOrder.customer.accountCredit >= 0:
        createOrderTaskChain(periodicOrder, True)
        periodicOrder.customer.save()
        periodicOrder.status = 0
        periodicOrder.save()
    else:
        pass  # TODO: inform user somehow?


def checkForPeriodicOrder():
    print 'checking'
    last5min = datetime.now() - timedelta(minutes=15)
    ordersForLast5Min = PeriodicOrder.objects.filter(
        status=10,
        deliveryTime__gt=last5min.time(),
        deliveryTime__lt=datetime.now().time()
    ).exclude(endDate__lt=datetime.now().togregorian().date())
    dayOfWeek = datetime.now().weekday()
    ordersToPlace = filter(lambda po: po.days[dayOfWeek] == True, ordersForLast5Min)
    print '%d orders found' % len(ordersToPlace)
    for po in ordersToPlace:
        placePeriodicOrder(po)


if __name__ == '__main__':
    schedule.every(1).minutes.do(checkForPeriodicOrder)

    while True:
        schedule.run_pending()
        django.db.close_old_connections()
        time.sleep(1)
