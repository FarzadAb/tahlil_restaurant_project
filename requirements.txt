Django==1.9.7
optional-django==0.3.0
react==3.0.1
jdatetime==1.8.1
persiantools==1.1.0
django-picklefield==0.3.2
schedule==0.3.2
