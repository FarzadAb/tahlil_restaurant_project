from main.models import Cook, Employee, ResourceManager, User, Cashier, Waiter, \
    Carrier, ParkingEmployee


def match_foods(all_foods,query):
    return all_foods

def isWaiter(employee):
    return Waiter.objects.filter(employee__id=employee.id).count()


def getParkingEmployee(request):
    return ParkingEmployee.objects.filter(employee__user__user__id=request.user.id).first()


def getWaiter(request):
    return Waiter.objects.filter(employee__user__user__id=request.user.id).first()


def getCarrier(request):
    return Carrier.objects.filter(employee__user__user__id=request.user.id).first()


def isCashier(request):
    return getCashier(request) is not None

def getCashier(request):
    return Cashier.objects.filter(employee__user__user__id=request.user.id).first()


def getEmployee(request):
    current_emps = Employee.objects.filter(user__user__id = request.user.id)
    if len(current_emps) > 0:
        return current_emps[0]
    return None


def getCook(request):
    return Cook.objects.filter(resourceManager__employee__user__user__id=request.user.id).first()


def getResourceManager(request):
    return ResourceManager.objects.filter(employee__user__user__id=request.user.id).first()


def getUser(request):
    return User.objects.filter(user_id=request.user.id).first()
