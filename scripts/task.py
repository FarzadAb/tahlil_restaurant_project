# -*- coding: utf-8 -*-
from main.models import Task, Carrier, Cook, Waiter, Review
from django.db.models import Count

def findNextCarrier(branch):
    return (Carrier.objects.filter(atWork=True, employee__branch__id=branch.id)
                   .annotate(num_tasks=Count('employee__task'))
                   .order_by('num_tasks').first())

def findNextWaiter(branch):
    return (Waiter.objects.filter(employee__branch__id=branch.id)
                  .annotate(num_tasks=Count('employee__task'))
                  .order_by('num_tasks').first())

def createOrderTaskChain(order, periodic=False, extra=None):
    print extra
    if periodic:
        finalStatus = 10
    else:
        finalStatus = 3
    if extra['isCashier']:
        Review.objects.create(
            order = order,
            employee = extra['cashier'].employee,
            content = u'گرفتن سفارش غذا'
        )
    if order.forHome:
        delivery = Task.objects.create(name='تحویل', order=order, next_task=None,
            order_status=finalStatus, employee=findNextCarrier(order.branch).employee,
            details=u'به آدرس: ' + order.customer.address)
    else:
        if 'tableNum' in extra and extra['tableNum']:
            delivery = Task.objects.create(name='تحویل', order=order, next_task=None,
                order_status=finalStatus, employee=findNextWaiter(order.branch).employee,
                details=u'به میز: ' + extra['tableNum'])
        elif extra['isCashier']:
            delivery = False
        else:
            delivery = Task.objects.create(name='تحویل', order=order, next_task=None,
                order_status=finalStatus, employee=findNextWaiter(order.branch).employee,
                details=u'به مشتری: ' + order.customer.name())
        if 'withChild' in extra and extra['withChild'] and delivery:
            delivery.details += u' (به همراه کودک)'
            delivery.save()
    bake = []
    print order.orderedfood_set.values_list('food__cook').distinct()
    for cookId in order.orderedfood_set.values_list('food__cook').distinct():
        cook = Cook.objects.get(id=cookId[0])
        ofs = order.orderedfood_set.filter(food__cook__id=cook.id)
        bake.append(Task.objects.create(
            name='پخت', order=order, order_status=1,
            employee=cook.resourceManager.employee,
            details='\n'.join([of.food.name.encode('utf-8') + ' × ' + str(of.count)
                for of in order.orderedfood_set.filter(food__cook__id=cook.id)])
        ))
    for i in range(len(bake)-1):
        bake[i].next_task = bake[i+1]
        bake[i].save()
    bake[0].active = True
    bake[0].save()
    bake[-1].order_status = 2
    if delivery == False:
        bake[-1].order_status = finalStatus
    else:
        bake[-1].next_task = delivery
    bake[-1].save()
