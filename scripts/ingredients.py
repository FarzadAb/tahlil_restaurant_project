# -*- coding: utf-8 -*-
from main.models import Ingredient, FoodIngredient
from persiantools import digits
import re


def getOrCreateIngredient(name, unit):
    ing = Ingredient.objects.filter(name=name).first()
    if ing:
        return ing
    return Ingredient.objects.create(name=name, unit=unit)


def trimSpaces(s):
    while len(s) and s[0] == ' ':
        s = s[1:]
    while len(s) and s[-1] == ' ':
        s = s[:-1]
    return s

def update_ingredients(food):
    keyword = u'مواد اولیه'
    s = food.properties
    if s.find(keyword) == -1:
        return
    s = s[s.find(keyword)+len(keyword):]
    s = s[s.find(u'\n')+1:]
    food.foodingredient_set.all().delete()
    for line in s.split(u'\n'):
        try:
            name, rest = line.split(':')
            name = trimSpaces(name)
            rest = digits.fa_to_en(trimSpaces(rest).encode('utf-8'))
            m = re.match(r'^[^\d]*(\d+)[^\d]', rest + 'a')
            if m:
                count = int(m.groups()[0])
                unit = trimSpaces(rest[m.end()-1:])
                ingred = getOrCreateIngredient(name, unit)
                FoodIngredient.objects.create(food=food, ingredient=ingred, count=count)
        except Exception, e:
            print e
            pass
