
treshold = 10 # The treshold to remove foods from menu if a cook is busy
def update_foods(cook):
    foods=cook.food_set.all()
    if len(foods) <= 0:
        return
    skills=map((lambda food:food.cookAbility),foods)
    max_skill= max(skills)
    ind_max=skills.index(max_skill)
    for food in foods:
        if len(cook.resourceManager.employee.task_set.filter(active=True)) > treshold and food.cookAbility < 3:
            food.isAvailable=False
        food.isChefSuggestion=False
    foods[ind_max].isChefSuggestion=True
    for food in foods:
        food.save()
