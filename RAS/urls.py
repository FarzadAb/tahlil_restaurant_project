"""RAS URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
import os
import time
from django.conf.urls import url
from django.contrib import admin

from views import loginPage, logoutPage, checkLogin, signupPage, checkSignup, index
from main.views import menu, menu_edit, menu_save, image_upload, showMyOrders, \
    cancelOrderCheck, cancelOrder, checkSubmitOrder, submitOrder, chargeAccount, seeCredit,\
    modify_resource, show_resources, new_resource, show_tasks, show_reports,clear_reports,\
    complete_task, profile, showOrder, submitReview, getReview, startWork, leaveWork, \
    menuSpecific, branches, getFoodReview, submitFoodReview, Manage_Employees, \
    parking, confirmParking #, profile_edit, profile_save

os.environ['TZ'] = 'Asia/Tehran'
time.tzset()

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', index, name='index'),
    url(r'^menu/$', menu , name='menu'),
    url(r'^branches/$', branches , name='branches'),
    url(r'^menu/(?P<branchId>[0-9]+)$', menuSpecific , name='menuSpecific'),
    url(r'^myOrders/$', showMyOrders , name='myOrders'),
    url(r'^order/(?P<orderId>[0-9]+)$', showOrder , name='showOrder'),
    url(r'^review/(?P<reviewId>[0-9]+)$', getReview , name='getReview'),
    url(r'^foodReview/(?P<reviewId>[0-9]+)$', getFoodReview , name='getFoodReview'),
    url(r'^submitReview/$', submitReview , name='submitReview'),
    url(r'^submitFoodReview/$', submitFoodReview , name='submitFoodReview'),
    url(r'^cancelOrderCheck/(?P<orderId>[0-9]+)$', cancelOrderCheck , name='cancelOrderCheck'),
    url(r'^cancelOrder/(?P<orderId>[0-9]+)$', cancelOrder , name='cancelOrder'),
    url(r'^checkSubmitOrder/$', checkSubmitOrder , name='checkSubmitOrder'),
    url(r'^submitOrder/$', submitOrder , name='submitOrder'),
    url(r'^parking/$', parking , name='parking'),
    url(r'^confirmParking/$', confirmParking , name='confirmParking'),
    url(r'^chargeAccount/$', chargeAccount , name='chargeAccount'),
    url(r'^seeCredit/$', seeCredit , name='seeCredit'),
    url(r'^signup/', signupPage, name='signupPage'),
    url(r'^checkSignup/', checkSignup, name='checkSignup'),
    url(r'^login/', loginPage, name='login'),
	url(r'^logout/', logoutPage, name='logout'),
	url(r'^checkLogin/', checkLogin, name='checkLogin'),
	url(r'^startWork/', startWork, name='startWork'),
	url(r'^leaveWork/', leaveWork, name='leaveWork'),
	url(r'^menu/edit/', menu_edit, name='menuEdit'),
	url(r'^menu/save/', menu_save, name='menuSave'),
	url(r'^image/upload/', image_upload, name='imageUpload'),
	url(r'^show/resources/', show_resources, name='showResources'),
	url(r'^modify/resource/', modify_resource, name='modifyResource'),
	url(r'^new/resource/', new_resource, name='newResource'),
	url(r'^show/tasks/$', show_tasks, name='showTasks'),
	url(r'^show/reports/$', show_reports, name='showReports'),
	url(r'^clear/reports/$', clear_reports, name='clearReports'),
	url(r'^complete/task/$', complete_task, name='completeTask'),
    url(r'^EmployeeManagemnet/$', Manage_Employees, name='EmployeeManagemnet'),
	# url(r'^report/add/$', report_add, name='reportAdd'),
   url(r'^profile/$', profile, name='profile'),
#	url(r'^profile/edit/', profile_edit, name='profileEdit'),
#	url(r'^profile/save/', profile_save, name='proflieSave'),
]
