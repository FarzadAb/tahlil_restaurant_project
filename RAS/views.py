from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.contrib.auth import models
from main.models import User
from scripts.search import getEmployee


def signupPage(request):
    logout(request)
    if len(request.GET.getlist('next')) < 1 :
        next = reverse('index')
    else:
        next = request.GET['next']
    return render( request , 'signup.html' , { 'next' : next } )


@csrf_protect
def checkSignup(request):
    try:
        authUser = models.User.objects.create_user(
            username=request.POST['username'],
            password=request.POST['password'],
            first_name=request.POST['firstName'],
            last_name=request.POST['lastName'],
            email=request.POST['email'],
        )
        user = User.objects.create(user=authUser, address=request.POST['address'])
    except Exception, e:
        print e
        return redirect('signupPage')
    authUser = authenticate( username = request.POST['username'] , password = request.POST['password'] )
    login(request, authUser)
    return redirect( request.POST['next'] )

def loginPage(request) :
    logout(request)
    if len(request.GET.getlist('next')) < 1 :
        next = reverse('index')
    else:
        next = request.GET['next']
    return render( request , 'login.html' , { 'next' : next } )


def logoutPage(request) :
    logout(request)
    return redirect( '/login' )


@csrf_protect
def checkLogin(request) :
    username = request.POST['username']
    password = request.POST['password']

    user = authenticate( username = username , password = password )
    print user
    if user is not None :
        sysUser = User.objects.filter(user__id=user.id).first()
        if sysUser is not None and user.is_active :
            login(request, user)
            return redirect( request.POST['next'] )
        else:
            return redirect( '/login/?next=%s' %(request.POST['next']) )
    else:
        return redirect( '/login/?next=%s' %(request.POST['next']) )


@login_required
def index(request):
    if getEmployee(request):
        return redirect('showTasks')
    return redirect('menu')
