# -*- coding: utf-8 -*-
# import django
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseForbidden
# from django.contrib import auth
from django.db import transaction
from django.db.models import Count
from django.utils import timezone
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponse
from django.shortcuts import redirect
from django.template import loader
from itertools import chain
import jdatetime

import sys
from sys import platform as _platform
from models import Food, User, Cook, Order, OrderedFood, Resource,\
        Report, Task, PeriodicOrder, Review, Branch, FoodReview, Discount, Employee, \
        ParkingReservation
from scripts.search import match_foods
from scripts.search import getCook, getResourceManager, getEmployee, getCashier, \
        isCashier, isWaiter, getCarrier, getParkingEmployee
from scripts.task import createOrderTaskChain
from scripts.autocheck import update_foods
from scripts.ingredients import update_ingredients
from main.statics import parkingNums

# from sys import stderr
import locale
# import logging

from statics import cancelReturn

if _platform == 'win32':
    pass
else:
    locale.setlocale(locale.LC_ALL, "fa_IR")


@login_required
def menu(request):
    if getEmployee(request):
        return redirect('menuSpecific', getEmployee(request).branch.id)
    user = User.objects.filter(user_id=request.user.id).first()
    if user is None:
        return redirect('login')
    if user.order_set.count():
        branchId = user.order_set.order_by('-submitTime').first().branch.id
        return redirect('menuSpecific', branchId)
    return redirect('branches')


@login_required
def branches(request):
    template = loader.get_template('main/branches.html')
    return HttpResponse(template.render({'branches': Branch.objects.all()}, request))



@login_required
def menuSpecific(request, branchId):
    user = User.objects.filter(user_id=request.user.id).first()
    if user is None:
        return redirect('login')
    branch = Branch.objects.filter(id=branchId).first()
    if branch is None:
        return redirect('branches')
    # Can this user edit menu
    can_edit=False
    current_cook= getCook(request)
    if current_cook is not None and current_cook.resourceManager.employee.branch.id == int(branchId):
        can_edit=current_cook.canEditMenu

    # What foods to show in menu
    all_foods = (Food.objects.filter(cook__resourceManager__employee__branch__id=branchId)
                     .annotate(num_tasks=Count('cook__resourceManager__employee__task'))
                     .filter(num_tasks__lt=5))
    if can_edit:
        all_foods=all_foods.all()
    else:
        all_foods=all_foods.filter(isAvailable=True,isInMenu=True)

    if 'q' not in request.GET:
        selected=all_foods
    else:
        selected=match_foods(all_foods,request.GET['q'])
    suggested=selected.filter(isSuggested=True)
    not_suggested=selected.filter(isSuggested=False)
    menu_foods=list(chain(suggested,not_suggested))
    template = loader.get_template('main/search.html')
    context = {'foods': menu_foods, 'can_edit': can_edit, 'branchId': branchId,
               'isCashier': isCashier(request) and (getCashier(request).employee.branch.id==int(branchId))}
    if not context['isCashier']:
        context['discounts'] = [d.preview() for d in
                user.discount_set.filter(used=False).order_by('-amount')[:5]]
    return HttpResponse(template.render(context, request))


# set get[food] with Food.name
@login_required
def menu_edit(request):
    template = loader.get_template('main/menuEdit.html')
    current_cook = getCook(request)
    if current_cook is None or not current_cook.canEditMenu:
        return HttpResponseForbidden()
    is_new=False
    if 'food' in request.GET:
        foods=Food.objects.filter(name=request.GET['food'])
        current_food=foods[0]
    else:
        current_food=Food()
        is_new=True
    context = {'food': current_food,'is_new': is_new}
    return HttpResponse(template.render(context, request))

@csrf_protect
@login_required
def menu_save(request):
    food=None
    foods= Food.objects.filter(name=request.POST['food_name'])

    if len(foods) > 0:
        food=foods[0]

    if 'new' in request.POST:
        if len(foods) > 0:
            #TODO redirect and send error
            return redirect(reverse('menu'))

        food=Food()
        current_cook= getCook(request)
        food.cook=current_cook

    else:
        if len(foods) <= 0:
            #TODO redirect and send error
            return redirect(reverse('menu'))

        if 'remove' in request.POST:
            food.delete()
            return redirect(reverse('menu'))
    if 'food_name' in request.POST:
        food.name=request.POST['food_name']
    if 'food_cookAbility' in request.POST:
        food.cookAbility=request.POST['food_cookAbility']
    if 'food_price' in request.POST:
        food.price=request.POST['food_price']
    if 'food_imgUrl' in request.POST:
        food.imgUrl=request.POST['food_imgUrl']
    if 'food_isAvailable' in request.POST:
        food.isAvailable=True
    else:
        food.isAvailable=False
    if 'food_isSuggested' in request.POST:
        food.isSuggested=True
    else:
        food.isSuggested=False
    if 'food_isInMenu' in request.POST:
        food.isInMenu=True
    else:
        food.isInMenu=False
    if 'food_properties' in request.POST:
        food.properties=request.POST['food_properties']
    food.save()
    update_ingredients(food)
    update_foods(food.cook)
    return redirect(reverse('menu'))

@login_required
def show_resources(request):
    cur_rm=getResourceManager(request)
    if cur_rm is None :
        redirect(reverse('menu'))
    resources=Resource.objects.filter(readLevel__lte=cur_rm.readAccess)

    template = loader.get_template('main/showResources.html')
    context = {'resources': resources,'cur_rm': cur_rm}
    return HttpResponse(template.render(context, request))


@login_required
def modify_resource(request):
    cur_rm=getResourceManager(request)
    if cur_rm is None :
        return redirect(reverse('menu'))

    resources=Resource.objects.filter(name=request.POST['resource_name'])
    if len(resources) <= 0:
        return redirect(reverse('showResources'))
    resource=resources[0]
    if resource.writeLevel > cur_rm.writeAccess :
        return redirect(reverse('showResources'))
    change=0
    if 'add' in request.POST:
        change=int(request.POST['value_change'])
    if 'sub' in request.POST:
        change=-int(request.POST['value_change'])
    resource.value += change
    resource.save()
    return redirect(reverse('showResources'))

@login_required
def new_resource(request):
    cur_rm=getResourceManager(request)
    if cur_rm is None :
        return redirect(reverse('menu'))
    resource=Resource()
    resource.name=request.POST['resource_name']
    resource.value=request.POST['resource_value']
    resource.readLevel=request.POST['resource_r']
    resource.writeLevel=request.POST['resource_w']
    if 'resource_properties' in request.POST:
        resource.properties=request.POST['resource_properties']
    resource.save()
    return redirect(reverse('showResources'))


@login_required
def image_upload(request):
    url=""
    #TODO upload the image and return the url for ajax
    return url


@login_required
def showMyOrders(request):
    template = loader.get_template('main/orders.html')
    customer = User.objects.filter(user_id=request.user.id).first()
    if customer is None :
        return redirect('login')  # change this
    activeOrders = filter(
        lambda o: not hasattr(o, 'periodicorder') or o.periodicorder.active(),
        customer.order_set.exclude(status=-1).order_by('-submitTime')
    )
    context = {
        'mine': True,
        'orders': [o.preview() for o in activeOrders],
    }
    return HttpResponse(template.render(context, request))


@login_required
def showOrder(request, orderId):
    template = loader.get_template('main/order.html')
    customer = User.objects.filter(user_id=request.user.id).first()
    if customer is None :
        return redirect('login')
    order = Order.objects.filter(id=int(orderId)).exclude(status=-1).first()
    if order is None:
        return redirect('menu')
    mine = order.customer.id == customer.id
    if not mine and getEmployee(request) is None:
        return redirect('menu')
    reviews = []
    foodReviews = []
    if mine:
        reviews = order.review_set.filter(valid=False)
        foodReviews = order.foodreview_set.filter(valid=False)
    context = {
        'mine': mine,
        'customer': customer,
        'foods': [of.preview() for of in order.orderedfood_set.all()],
        'order': order.preview(),
        'isCashier': getCashier(request),
        'reviews': reviews,
        'foodReviews': foodReviews,
    }
    return HttpResponse(template.render(context, request))


@login_required
def cancelOrderCheck(request, orderId):
    template = loader.get_template('main/cancelOrderCheck.html')
    order = Order.objects.filter(id=int(orderId)).first()
    if order is None:
        return redirect('myOrders')
    if order.status == 10:
        return redirect('cancelOrder', order.id)
    context = {
        'order': order.preview(),
        'type': 'Order',
    }
    return HttpResponse(template.render(context, request))


@transaction.atomic
def doCancelOrder(orderId, user, doReturn=True):
    order = Order.objects.filter(id=int(orderId)).first()
    if order and (order.customer.id == user.id or getCashier(request)):
        if doReturn:
            customer = order.customer
            customer.accountCredit += order.totalPrice() * cancelReturn[order.status]
            customer.save()
        order.cancelationTime = timezone.localtime(timezone.now())
        order.status = -1
        order.save()
        order.task_set.all().delete()


@login_required
def cancelOrder(request, orderId):
    user = User.objects.filter(user_id=request.user.id).first()
    if user is None:
        return redirect('login')
    doCancelOrder(orderId, user)
    return redirect('myOrders')


@csrf_protect
@login_required
def checkSubmitOrder(request):
    keyword = 'foodId'
    template = loader.get_template('main/checkSubmitOrder.html')
    orderList = []
    totalPrice = 0
    print request.POST
    if 'username' in request.POST and request.POST['username']:
        if request.POST['action'] == 'periodic':
            return redirect('menu')
        user = User.objects.filter(user__username=request.POST['username']).first()
        if user is None:
            return redirect('menu')
    else:
        user = User.objects.filter(user_id=request.user.id).first()
        if user is None:
            return redirect('login')
    if request.POST['action'] == 'periodic':
        periodic = True
    else:
        periodic = False
    discount = 0
    if 'discountOrNot' in request.POST and int(request.POST['discountOrNot']) == 1:
        if 'discountId' in request.POST:
            discount = Discount.objects.filter(
                id=int(request.POST['discountId']),
                used=False,
                customer__user__id=user.user.id
            ).first()
            if discount:
                discount = discount.amount
            else:
                discount = 0
    for key in request.POST:
        if key[:len(keyword)] == keyword:
            food = Food.objects.filter(id=int(key[len(keyword):])).first()
            num = int(request.POST[key])
            if food is not None and num > 0:
                orderList.append({
                    'id': food.id,
                    'name': food.name,
                    'num': num,
                    'price': food.price,
                    'totalPrice': food.price * num,
                })
                totalPrice += food.price * num
    if len(orderList) == 0:
        return redirect('menu')
    context = {
        'branchId': request.POST['branchId'],
        'username': user.user.username,
        'isCashier': isCashier(request) and getCashier(request).employee.user.id != user.id,
        'order': orderList,
        'totalPrice': totalPrice,
        'needCharge': max(0, totalPrice * (1-discount) - user.accountCredit),
        'periodic': periodic,
        'startDate': jdatetime.datetime.fromgregorian(datetime=timezone.localtime(timezone.now())).strftime('%Y/%m/%d'),
        'time': jdatetime.datetime.fromgregorian(datetime=timezone.localtime(timezone.now())).strftime('%H:%M'),
    }
    if discount:
        context['afterDiscount'] = int(totalPrice * (1-discount))
        context['discountId'] = int(request.POST['discountId'])
    return HttpResponse(template.render(context, request))


@login_required
def getReview(request, reviewId):
    template = loader.get_template('main/getReview.html')
    user = User.objects.filter(user_id=request.user.id).first()
    if user is None:
        return redirect('login')
    review = Review.objects.filter(id=int(reviewId)).first()
    if review is None or review.order.customer != user:
        return redirect('myOrders')
    if review.valid:
        return redirect('showOrder', review.order.id)
    context = {
        'review': review,
    }
    return HttpResponse(template.render(context, request))


@login_required
def getFoodReview(request, reviewId):
    template = loader.get_template('main/getReview.html')
    user = User.objects.filter(user_id=request.user.id).first()
    if user is None:
        return redirect('login')
    review = FoodReview.objects.filter(id=int(reviewId)).first()
    if review is None or review.order.customer != user:
        return redirect('myOrders')
    if review.valid:
        return redirect('showOrder', review.order.id)
    context = {
        'review': review,
        'foodReview': True,
    }
    return HttpResponse(template.render(context, request))


@csrf_protect
@login_required
def submitFoodReview(request):
    user = User.objects.filter(user_id=request.user.id).first()
    if user is None:
        return redirect('login')
    review = FoodReview.objects.filter(id=int(request.POST['reviewId'])).first()
    if review is None or review.order.customer != user:
        return redirect('myOrders')
    if not review.valid:
        review.quality = int(request.POST['quality'])
        review.content = request.POST['content']
        review.valid = True
        review.save()
    return redirect('showOrder', review.order.id)


@csrf_protect
@login_required
def submitReview(request):
    user = User.objects.filter(user_id=request.user.id).first()
    if user is None:
        return redirect('login')
    review = Review.objects.filter(id=int(request.POST['reviewId'])).first()
    if review is None or review.order.customer != user:
        return redirect('myOrders')
    if not review.valid:
        review.quality = int(request.POST['quality'])
        review.content = request.POST['content']
        review.valid = True
        review.save()
    return redirect('showOrder', review.order.id)


@transaction.atomic
def placeOrder(order, customer, totalPrice, periodic=False, extra=None):
    discount = 0
    if extra['discount']:
        discount = extra['discount'].amount
    if not periodic:
        customer.accountCredit -= totalPrice * (1-discount)
    if customer.accountCredit >= 0:
        if not periodic:
            customer.save()
            o = Order.objects.create(customer=customer)
        else:
            o = PeriodicOrder.objects.create(customer=customer, status=10,
                startDate=extra['start_date'], endDate=extra['end_date'],
                deliveryTime=extra['time'], days=extra['days'])
        o.withChild = extra['withChild']
        o.forHome = extra['takeHome']
        o.branch_id = extra['branchId']
        o.discount = extra['discount']
        o.save()
        for item in order:
            OrderedFood.objects.create(order=o, food=item['food'], count=item['num'])
        if not periodic:
            createOrderTaskChain(o, extra=extra)
    else:
        raise Exception('not enough credit')


@csrf_protect
@login_required
def submitOrder(request):
    keyword = 'foodId'
    dayKeyword = 'day'
    orderList = []
    order = []
    extra = {
        'isCashier': False,
        'takeHome': ('takeHome' in request.POST),
        'branchId': int(request.POST['branchId']),
    }
    extra['withChild'] = (not extra['takeHome']) and ('withChild' in request.POST)
    if 'username' in request.POST:
        extra['isCashier'] = True
        extra['cashier'] = getCashier(request)
        extra['tableNum'] = request.POST['tableNum']
        user = User.objects.filter(user__username=request.POST['username']).first()
        if user is None:
            return redirect('menu')
    else:
        user = User.objects.filter(user_id=request.user.id).first()
        if user is None:
            return redirect('login')
    discount = None
    if 'discountId' in request.POST:
        discount = Discount.objects.filter(
            id=int(request.POST['discountId']),
            used=False,
            customer__user__id=user.user.id
        ).first()
        if discount:
            discount.used = True
            discount.save()
    extra['discount'] = discount
    totalPrice = 0
    periodic = False
    if request.POST['periodic'] == 'True':
        periodic = True
        extra['days'] = [False] * 7
        extra['start_date'] = jdatetime.date(
            *map(int, request.POST['start_date'].split('/'))).togregorian()
        if request.POST['end_date'].count('/') != 2:
            extra['end_date'] = None
        else:
            extra['end_date'] = jdatetime.date(
                *map(int, request.POST['end_date'].split('/'))).togregorian()
        extra['time'] = jdatetime.time(*map(int, request.POST['time'].split(':')))
    for key in request.POST:
        if key[:len(keyword)] == keyword:
            food = Food.objects.filter(id=int(key[len(keyword):])).first()
            num = int(request.POST[key])
            if food is not None and num > 0:
                totalPrice += num * food.price
                order.append({'food': food, 'num': num})
        elif periodic and key[:len(dayKeyword)] == dayKeyword:
            extra['days'][int(key[len(dayKeyword):])] = True

    try:
        placeOrder(order, user, totalPrice, periodic, extra)
    except Exception, e:
        print e
        template = loader.get_template('main/submitError.html')
        return HttpResponse(template.render({}, request))
    if 'username' in request.POST:
        return redirect('menu')
    return redirect('myOrders')


@csrf_protect
@login_required
def chargeAccount(request):
    if 'amount' not in request.POST:
        return redirect('seeCredit')
    amount = int(request.POST['amount'])
    context = {'amount': amount}
    if 'username' in request.POST:
        user = User.objects.filter(user__username=request.POST['username']).first()
        context['username'] = request.POST['username']
        if user is None:
            return redirect('menu')
    else:
        user = User.objects.filter(user_id=request.user.id).first()
        if user is None:
            return redirect('login')
    user.accountCredit += amount
    user.save()
    chargesTillNow = sum([r.value for r in Report.objects.filter(type='cash',
            message='user:'+str(user.user.id), valid=True)])
    discounts = int((chargesTillNow+amount) / 200) - int(chargesTillNow / 200)
    for i in range(discounts):
        Discount.objects.create(customer=user)
    discounts = int((chargesTillNow+amount) / 1000) - int(chargesTillNow / 1000)
    for i in range(discounts):
        Discount.objects.create(customer=user, amount=.5)
    Report.objects.create(type='cash', value=amount, valid=True,
                          message='user:'+str(user.user.id), task=None)
    template = loader.get_template('main/chargeAccount.html')
    return HttpResponse(template.render(context, request))


@login_required
def seeCredit(request):
    user = User.objects.filter(user_id=request.user.id).first()
    if user is None:
        return redirect('login')
    template = loader.get_template('main/seeCredit.html')
    return HttpResponse(template.render({'credit': user.accountCredit}, request))


@login_required
def profile(request):
    user = User.objects.filter(user_id=request.user.id).first()
    if user is None:
        return redirect('login')
    template = loader.get_template('main/profile.html')
    cook = getCook(request)
    resManager = getResourceManager(request)
    employee = getEmployee(request)
    if employee:
        title = employee.title()
    else:
        title = 'مشتری'
    context = {
                 'cur_cook': cook,
                 'cur_rm': getResourceManager(request),
                 'cur_car': getCarrier(request),
                 'cur_usr': user,
                 'cur_emp': employee,
                 'title': title,
                 'credit': user.accountCredit,
             }
    return HttpResponse(template.render(context, request))


@login_required
def startWork(request):
    carrier = getCarrier(request)
    if carrier is not None:
        carrier.atWork = True
        carrier.save()
    return redirect('index')


@login_required
def leaveWork(request):
    carrier = getCarrier(request)
    if carrier is not None:
        carrier.atWork = False
        carrier.save()
    return redirect('index')


@login_required
def show_tasks(request):
    employee = getEmployee(request)
    if employee is None:
        return redirect(reverse('menu'))
    tasks=employee.task_set.filter(active=True)
    manager=employee.manager
    template = loader.get_template('main/showTasks.html')
    context={
        'manager':manager,
        'tasks':tasks
    }
    return HttpResponse(template.render(context, request))


@login_required
def complete_task(request):
    pk=int(request.POST['task'])
    task=Task.objects.all().filter(pk=pk).first()
    if task is None:
        return redirect('showTasks')
    title = task.name + u' ' + task.details
    if not isWaiter(task.employee):
        Review.objects.create(
            order = task.order,
            employee = task.employee,
            content = title
        )
    if task.order_status == 3 or task.order_status == 10:
        ingreds = {}
        Report.objects.create(type='sell', value=task.order.totalPrice(), valid=True, task=None)
        for of in task.order.orderedfood_set.all():
            Report.objects.create(type='foodSold', value=of.count, valid=True, task=None)
            FoodReview.objects.create(
                order = task.order,
                food = of.food
            )
            for foodingred in of.food.foodingredient_set.all():
                if foodingred.ingredient.id in ingreds:
                    ingreds[foodingred.ingredient.id][0] += foodingred.count * of.count
                else:
                    ingreds[foodingred.ingredient.id] = (foodingred.count * of.count, foodingred.ingredient)
        for ingredId in ingreds:
            Report.objects.create(type='ingredientSold', value=ingreds[ingredId][0],
                                  valid=True, task=None)
            ingreds[ingredId][1].inStock -= ingreds[ingredId][0]
            ingreds[ingredId][1].save()

    employee = getEmployee(request)
    if employee is None:
        redirect(reverse('menu'))
    if employee.pk == task.employee.pk:
        task.complete()
    return redirect(reverse('showTasks'))


@login_required
def show_reports(request):
    rm = getResourceManager(request)
    if rm is None or rm.readAccess is False:
        return redirect(reverse('menu'))
    reports = Report.objects.all()
    if 'page' in request.GET:
        page = request.GET['page']
    else:
        page = 0

    result_type='cash'
    if 'report_type' in request.POST:
        result_type=request.POST['report_type']

    results=Report.objects.filter(type=result_type)
    result_message=''
    if  'report_message' in request.POST and len(request.POST['report_message']) > 0:
        result_message=request.POST['report_message']
        results=results.filter(message=result_message)

    result_value=sum(map(lambda x:x.value,results))

    reports = reports[50*page:50*(page+1)]
    template = loader.get_template('main/showReports.html')
    context = {
        'reports': reports,
        'result_name': result_type,
        'result_value': result_value,
        'result_message': result_message,
        'page': page
    }
    return HttpResponse(template.render(context, request))


@login_required
def clear_reports(request):
    rm = getResourceManager(request)
    if rm is None or rm.writeAccess is False:
        return redirect(reverse('menu'))
    Report.objects.all().delete()
    return redirect(reverse('showReports'))


@login_required
def Manage_Employees(request):
    rm = getResourceManager(request)
    if rm is None or getEmployee(request).manager is False:
        return redirect(reverse('menu'))
    reports = Report.objects.all()
    if 'page' in request.GET:
        page = request.GET['page']
    else:
        page = 0

    result_type='cash'
    if 'report_type' in request.POST:
        result_type=request.POST['report_type']

    results=Report.objects.filter(type=result_type)
    result_message=''
    if  'report_message' in request.POST and len(request.POST['report_message']) > 0:
        result_message=request.POST['report_message']
        results=results.filter(message=result_message)

    result_value=sum(map(lambda x:x.value,results))
    employees = Employee.objects.filter(branch=getEmployee(request).branch)

    reports = reports[50*page:50*(page+1)]
    template = loader.get_template('main/Employees.html')
    context = {
        'employees': employees,
        'page': page
    }
    return HttpResponse(template.render(context, request))



@login_required
def parking(request):
    user = User.objects.filter(user_id=request.user.id).first()
    if user is None:
        return redirect('login')
    parkingEmployee = getParkingEmployee(request)
    if parkingEmployee is not None:
        parkings = ParkingReservation.objects.filter(day=timezone.localtime(timezone.now()).date())
    else:
        parkings = user.parkingreservation_set.filter(day__gte=timezone.localtime(timezone.now()).date())
    template = loader.get_template('main/parking.html')
    return HttpResponse(template.render({
        'parkings': parkings,
        'parkingEmployee': parkingEmployee,
        'day': jdatetime.datetime.fromgregorian(datetime=timezone.localtime(timezone.now())).strftime('%Y/%m/%d'),
    }, request))


@login_required
def confirmParking(request):
    user = User.objects.filter(user_id=request.user.id).first()
    if user is None:
        return redirect('login')
    template = loader.get_template('main/confirmParking.html')
    hour = int(request.POST['hour'])
    day = jdatetime.date(*map(int, request.POST['day'].split('/'))).togregorian()
    reserveds = ParkingReservation.objects.filter(hour=hour, day=day)
    free = [True] * parkingNums
    for r in reserveds:
        free[r.place-1] = False
    for i in range(parkingNums):
        if free[i]:
            ParkingReservation.objects.create(customer=user, day=day, hour=hour, place=i+1)
            return HttpResponse(template.render({'found': True}, request))
    return HttpResponse(template.render({'found': False}, request))


# @login_required
# def clear_reports(request):
#     rm = getResourceManager(request)
#     if rm is None or rm.writeAccess is False:
#         return redirect(reverse('menu'))
#     Report.objects.all().delete()
#     return redirect(reverse('showReports'))
#


''' It seems that user has nothing to edit  ````` ````
@login_required
def profile_edit(request):
    user = User.objects.filter(user_id=request.user.id).first()
    if user is None:
        return redirect('login')
    template = loader.get_template('main/profileEdit.html')

    context={'cur_cook': getCook(request),
             'cur_rm': getResourceManager(request),
             'cur_usr': user}
    return HttpResponse(template.render(context, request))

@login_required
def profile_save(request):
    cook=getCook(request)
    cur_rm=getResourceManager(request)
    user=getUser(request)

    if user is None:
        redirect(reverse('menu'))

    if cur_rm is not None:
           if len(request.POST['user_password']) > 0:
        user.;


    return redirect(reverse('profile'))
'''
