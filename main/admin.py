from django.contrib import admin
from models import Food, User, Employee, Cook, Order, OrderedFood, PeriodicOrder, \
    ResourceManager, Resource, Report, Task, Carrier, Cashier, Waiter, Branch, ParkingEmployee

admin.site.register(Food)
admin.site.register(User)
admin.site.register(Employee)
admin.site.register(ResourceManager)
admin.site.register(Resource)
admin.site.register(Cook)
admin.site.register(Order)
admin.site.register(OrderedFood)
admin.site.register(PeriodicOrder)
admin.site.register(Report)
admin.site.register(Task)
admin.site.register(Carrier)
admin.site.register(Cashier)
admin.site.register(Waiter)
admin.site.register(Branch)
admin.site.register(ParkingEmployee)
