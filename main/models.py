# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib import auth
from django.utils import timezone
from django.core.validators import MinValueValidator
from statics import orderTypes, cancelReturn, reportTypes, parkingPlaceChoices, parkingHourChoices
from datetime import datetime as gdt
from jdatetime import datetime as jdt
from persiantools import digits
from scripts.autocheck import  update_foods
from picklefield.fields import PickledObjectField


# az rooye dfd model ha ro tayin konid
# 11 ta datastorage darim har kodomeshun ye model mishand

class User(models.Model):
    user = models.ForeignKey(auth.models.User, default=1)
    address = models.TextField(default='')
    createTime = models.DateTimeField(auto_now_add=True)
    lastModification = models.DateTimeField(auto_now=True)
    accountCredit = models.PositiveIntegerField(default=0, validators=[MinValueValidator(0)])

    def __unicode__(self):
        return self.user.username

    def name(self):
        return self.user.first_name + ' ' + self.user.last_name


class Branch(models.Model):
    name = models.CharField(max_length=100)
    address = models.TextField(default='')
    serviceRegion = models.TextField(default='')
    manager = models.ForeignKey(to='employee', null=True, blank=True, related_name='branchManager')


class Employee(models.Model):
    user = models.ForeignKey(to='user', default=1)
    employeeId = models.IntegerField(unique=True, db_index=True)  # do we need this? user already has an ID
    branch = models.ForeignKey(to='branch', default=1)
    salary = models.IntegerField()
    birthDate = models.DateField()
    manager = models.BooleanField(default=0)

    def title(self):
        cook = Cook.objects.filter(resourceManager__employee__id=self.id).first()
        if cook:
            if cook.isChef:
                return u'سرآشپز'
            else:
                return u'آشپز'
        elif ResourceManager.objects.filter(employee__id=self.id).count():
            return u'مدیر منابع'
        elif Carrier.objects.filter(employee__id=self.id).count():
            return u'پیک'
        elif Cashier.objects.filter(employee__id=self.id).count():
            return u'صندوقدار'
        elif Waiter.objects.filter(employee__id=self.id).count():
            return u'پیشخدمت'
        else:
            return u'کارمند'

    def __unicode__(self):
        return self.user.user.username + ' (' + str(self.employeeId) + ')'



class ResourceManager(models.Model):
    employee = models.ForeignKey(to='employee', default=1)
    readAccess = models.BooleanField(default=0)
    writeAccess = models.BooleanField(default=0)

    def __unicode__(self):
        return self.employee.user.user.username + ' (' + str(self.employee.employeeId) + ')'


class Cook(models.Model):
    resume = models.TextField(default='')
    resourceManager = models.ForeignKey(to='resourceManager', default=1)
    isChef = models.BooleanField(default=False)
    canEditMenu = models.BooleanField(default=False)

    def __unicode__(self):
        return self.resourceManager.employee.user.user.username + ' (' + str(self.resourceManager.employee.employeeId) + ')'


#merge this one with user
#class Customer(User):
#    customerId = models.IntegerField(unique=True, db_index=True)  # do we need this? user already has an ID
#
#    def __unicode__(self):
#        return self.username + ' (' + str(self.customerId) + ')'

class Ingredient(models.Model):
    name = models.CharField(max_length=100, db_index=True)
    unit = models.CharField(max_length=100)
    inStock = models.IntegerField(default=0)


class FoodIngredient(models.Model):
    food = models.ForeignKey(to='food')
    ingredient = models.ForeignKey(to='ingredient')
    count = models.IntegerField(default=1)


class Food(models.Model):
    name = models.CharField(max_length=100, unique=True)
    price = models.IntegerField()
    imgUrl = models.URLField(blank=True)
    cookAbility = models.IntegerField(default=5)
    cook = models.ForeignKey(Cook,default=1)
    isAvailable = models.BooleanField(default=True)
    isInMenu= models.BooleanField(default=True)
    isSuggested = models.BooleanField(default=False)
    isChefSuggestion= models.BooleanField(default=False)
    properties = models.TextField(default='')

    def __unicode__(self):
        return self.name

    def reviewsStr(self):
        reviews = self.foodreview_set.filter(valid=True)
        if reviews.count() == 0:
            return ''
        avg = sum([r.quality for r in reviews]) * 1.0 / reviews.count()
        return (u'میانگین:' + unicode(str(avg)) + u'\n'
                + '\n'.join([u'نظر: ' + r.content for r in reviews[:2]]))


class Resource(models.Model):
    name = models.CharField(max_length=100, unique=True)
    value = models.IntegerField()
    writeLevel = models.IntegerField(default=1)
    readLevel = models.IntegerField(default=1)
    properties = models.TextField()  # isn't it better to call this details or something?

    def __unicode__(self):
        return self.name


class Order(models.Model):
    customer = models.ForeignKey(to='user')
    branch = models.ForeignKey(to='branch', default=1)
    withChild = models.BooleanField(default=False)
    forHome = models.BooleanField(default=True)
    submitTime = models.DateTimeField(auto_now_add=True)
    lastModification = models.DateTimeField(auto_now=True)
    status = models.IntegerField(choices=orderTypes, default=0, db_index=True)
    cancelationTime = models.DateTimeField(blank=True, auto_now=True)
    discount = models.ForeignKey(to='discount', null=True, blank=True, default=None)

    def isCanceled(self):
        return self.status == -1

    def totalPrice(self):
        ratio = 1
        if self.discount != None:
            ratio = 1 - self.discount.amount
        return sum([of.food.price * of.count for of in self.orderedfood_set.all()]) * ratio

    def preview(self):
        try:
            periodic = self.periodicorder
        except:
            periodic = None
        return {
            'id': self.id,
            'customer': self.customer,
            'time': digits.en_to_fa(jdt.fromgregorian(datetime=self.submitTime)
                          .strftime('%a، %d %b %Y ساعت %H:%M:%S').encode('utf-8')),
            'status': self.get_status_display(),
            'withChild': self.withChild,
            'forHome': self.forHome,
            'totalPrice': self.totalPrice(),
            'cancel': cancelReturn[self.status] * 100,
            'periodic': periodic,
        }

    def __unicode__(self):
        return 'Order [' + self.customer.__unicode__() + ', ' + str(self.id) + ']'



class Task(models.Model):
    name=models.TextField(default='')
    details=models.TextField(default='')
    order=models.ForeignKey(to='order',null=True,blank=True)
    order_status=models.IntegerField(default=0)
    next_task=models.ForeignKey(to='Task',null=True,blank=True)
    employee=models.ForeignKey(to='employee')
    active=models.BooleanField(default=False)  # all tasks are added simultaneously

    def activate(self):
        self.active=True
        if self.order is not None :
            self.order.status=self.order_status
            self.save()
        cook=Cook.objects.filter(resourceManager__employee__pk=self.employee.pk).first()
        if cook is not None:
            update_foods(cook)

    def complete(self):
        for report in self.report_set.all():
            report.publish()
        if self.next_task is not None:
            self.next_task.activate()
        self.order.status = self.order_status
        self.order.save()
        self.delete()


class Report(models.Model):
    type=models.CharField(default='', max_length=100, choices=reportTypes, db_index=True)
    value=models.IntegerField(default=0)
    message=models.TextField(default='', db_index=True)
    valid=models.BooleanField(default=False)  # reports are not valid until their task is completed
    task=models.ForeignKey(to='Task',null=True,blank=True)
    def publish(self):
        self.valid=True
        self.save()


class PeriodicOrder(Order):
    days = PickledObjectField(default=(False,)*7)
    startDate = models.DateField()
    endDate = models.DateField(null=True, blank=True, db_index=True)
    deliveryTime = models.TimeField(db_index=True)

    def __unicode__(self):
        return 'PeriodicOrder [' + self.customer.__unicode__() + ', ' + str(self.id) + ']'

    def active(self):
        return (self.endDate is None) or (self.endDate >= timezone.localtime(timezone.now()).date())

    def daysStr(self):
        return u'، '.join([jdt.j_weekdays_fa[d[1]] for d in filter(lambda d: d[0], zip(self.days, range(7)))])

    def startDateStr(self):
        return (jdt.fromgregorian(datetime=gdt.fromordinal(self.startDate.toordinal()))
                  .date().isoformat().replace('-','/'))

    def endDateStr(self):
        if self.endDate is None:
            return 'نامشخص'
        return (jdt.fromgregorian(datetime=gdt.fromordinal(self.endDate.toordinal()))
                  .date().isoformat().replace('-','/'))


class OrderedFood(models.Model):
    food = models.ForeignKey(to='food')
    order = models.ForeignKey(to='order')
    count = models.IntegerField(default=1)

    def preview(self):
        return {
            'foodName': self.food.name,
            'foodPrice': self.food.price,
            'totalPrice': self.count * self.food.price,
            'count': self.count,
        }


class Cashier(models.Model):
    employee = models.ForeignKey(to='employee', default=1)
    cashRegisterNumber = models.IntegerField(default=1)


class Waiter(models.Model):
    employee = models.ForeignKey(to='employee', default=1)


class Carrier(models.Model):
    employee = models.ForeignKey(to='employee', default=1)
    vehicle = models.IntegerField(choices=(zip(range(4), ('car', 'motorcycle', 'bike', 'van'))))
    atWork = models.BooleanField(default=True)

    def __unicode__(self):
        return 'Carrier (' + self.get_vehicle_display() + ')'


class ParkingEmployee(models.Model):
    employee=models.ForeignKey(to='employee', default=1)


class Review(models.Model):
    order = models.ForeignKey(to='order')
    employee = models.ForeignKey(to='employee')
    valid = models.BooleanField(default=False)
    quality = models.IntegerField(blank=True, null=True)
    content = models.TextField(blank=True)


class FoodReview(models.Model):
    order = models.ForeignKey(to='order')
    food = models.ForeignKey(to='food')
    valid = models.BooleanField(default=False)
    quality = models.IntegerField(blank=True, null=True)
    content = models.TextField(blank=True)


class Discount(models.Model):
    customer = models.ForeignKey(to='user')
    amount = models.FloatField(default=0.1)
    used = models.BooleanField(default=False)

    def code(self):
        return 'c' + str(self.customer.id) + 'p' + str(self.id)

    def preview(self):
        return {
            'id': self.id,
            'code': self.code(),
            'percent': int(self.amount * 100),
        }


class ParkingReservation(models.Model):
    customer = models.ForeignKey(to='user', default=1)
    day = models.DateField(db_index=True)
    hour = models.IntegerField(choices=parkingHourChoices, db_index=True)
    place = models.IntegerField(choices=parkingPlaceChoices, db_index=True)

    def dayStr(self):
        return (jdt.fromgregorian(datetime=gdt.fromordinal(self.day.toordinal()))
                  .date().isoformat().replace('-','/'))

    class Meta:
        unique_together = ('day', 'hour', 'place')
