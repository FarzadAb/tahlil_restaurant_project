# -*- coding: utf-8 -*-
from persiantools import digits

orderTypes = (
    (-1, u'کنسل‌شده'),
    (0, u'ثبت‌شده'),
    (1, u'نیمه‌آماده'),
    (2, u'در حال تحویل'),
    (3, u'تحویل داده‌شده'),
    (10, u'سفارش دوره‌ای (انتظار)'),
)

cancelReturn = {
    0: .8,
    1: .3,
    2: .1,
    3: 0,
    10: 0,
}

reportTypes = (
    ('cash', u'شارژ حساب'),
    ('sell', u'میزان فروش'),
    ('foodSold', u'غذا فروخته شده'),
    ('ingredientSold', u'مواد اولیه استفاده شده'),
)

parkingNums = 10
parkingPlaceChoices = zip(range(1, parkingNums+1), [digits.en_to_fa(str(p)) for p in range(1, parkingNums+1)])

parkingFee = 1

parkingHours = range(6, 23)
parkingHourChoices = zip(parkingHours,
    [u'ساعت ' + unicode(digits.en_to_fa(str(h)), 'utf-8') for h in parkingHours])
